var request = require('superagent');
var expect = require('expect.js');
var server = 'http://localhost:3000/api';

describe('authentication', function () {
    var registerObj = {
        email: "test@localhost",
        password: "password",
        passwordAgain: "password",
        name: "test testsson",
        nick: "test",
        ssn: "2403902139",
        phone: "5812345"
    };


    var loginObj = {
        email: 'test@localhost',
        password: 'password'
    };

    it('should creata a user using a normal user creation', function (done) {
        request
            .post(server + '/users')
            .send(registerObj)
            .end(function (res) {
                expect(res.status).to.equal(200);
                done();
            });
    });


    it('should fail at creating a user because of wrong SSN', function(done){

        registerObj.ssn = '2401914134';

        request
            .post(server + '/users')
            .send(registerObj)
            .end(function (res) {
                expect(res.status).to.equal(400);
                done();
            });
    });


    it('should login a user created', function (done) {
        request
            .post(server + '/session')
            .send(loginObj)
            .end(function(res){
                expect(res.status).to.equal(200);
                done();
            })
    })
});

describe('Games', function () {
    it('should return a list of games', function (done) {
        request.get('localhost:3000/api/game').end(function (res) {
            expect(res.status).to.equal(200);
            expect(res.body).to.be.an('array');
            done();
        });
    });
});