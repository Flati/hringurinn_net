var gulp = require('gulp'),
	inject = require('gulp-inject'),
	nodemon = require('gulp-nodemon'),
	less = require('gulp-less'),
	watch = require('gulp-watch'),
	ngmin = require('gulp-ngmin'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	css_minify = require('gulp-minify-css'),
	mocha = require('gulp-mocha');


gulp.task('demon', function () {
	nodemon({
		script: 'server.js',
		ext: 'js less png jpg',
		ignore: ['dist/**', '.idea/**', 'bower_components/**', 'node_modules/**'],
		env: {
			'NODE_ENV': 'development'
		},
		"verbose": true
	})
		.on('start', ['app-inject', 'html', 'images', 'less', 'js'])
		.on('change', ['html', 'less'])
		.on('restart', function () {
			console.log('restarted!');
		});
});

gulp.task('app-inject', function () {
	gulp.src('./app/index.html')
		.pipe(inject(gulp.src('./app/modules/**/*.js', {read: false}), {
			ignorePath: '/app',
			addPrefix: 'assets'
		}))
		.pipe(gulp.dest('./dist/'));
});


gulp.task('app-inject-prod', function () {
	gulp.src('./app/index.html')
		.pipe(inject(gulp.src('./dist/assets/modules/all.js', {read: false}), {
			ignorePath: '/dist/assets',
			addPrefix: 'assets'
		}))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('test', function () {
	setTimeout(function () {
		gulp.src('./test/*.js')
			.pipe(mocha({reporter: 'list'}));
	}, 2000);
});

gulp.task('html', function () {
	console.log('HTML');
	gulp.src(['./app/**/*.html', '!./**/*index.html'])
		.pipe(gulp.dest('./dist/assets'));
});

gulp.task('images', function () {
	gulp.src(['./app/images/*'])
		.pipe(gulp.dest('./dist/assets/images'));
});

gulp.task('watch', function () {
	//gulp.watch(['./app/**/*.html'], ['html']);
	//gulp.watch(['./app/less/**/*.less'], ['less']);
	//gulp.watch(['./app/**/*.js'], ['app-inject', 'js']);
	//gulp.watch(['./app/images/*'], ['images']);
});

gulp.task('js', function () {
	gulp.src('./app/modules/**/*.js')
		.pipe(gulp.dest('./dist/assets/modules/'))
});

gulp.task('less', function () {
	gulp.src('./app/less/styles.less')
		.pipe(less())
		.pipe(css_minify())
		.pipe(gulp.dest('./dist/assets/css'));
});

gulp.task('ngmin', function () {
	gulp.src('./app/modules/**/*.js')
		.pipe(concat('all.js'))
		.pipe(ngmin({dynamic: true}))
		.pipe(uglify({mangle: false}))
		.pipe(gulp.dest('./dist/assets/modules/'));
});


gulp.task('default', ['demon']);

gulp.task('prod', ['app-inject-prod', 'html', 'images', 'less', 'ngmin']);