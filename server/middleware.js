'use strict';

var _ = require('lodash'),
    moment = require('moment'),
    services = require('./services/services');

/**
 * Custom middleware used by the application
 */
module.exports = {

    /**
     *  Protect routes on your api from unauthenticated access
     */
    auth: function auth(req, res, next) {
        if (req.isAuthenticated()) return next();
        res.send(401);
    },

    roles: function (roles) {
        return roles[roles] || (roles[roles] = function (req, res, next) {
            if (_.find(roles, function (role) {
                return req.user.role === role;
            })) {
                return next();
            }
            return res.send(401);
        });
    },
    checkIfRegistrationClosed : function(req, res, next){
        services.getSettings().then(function(settings){
            if(settings.register){
                return settings.register ? next() : res.send(410);
            }
            else {
                next();
            }
        });
    }
};