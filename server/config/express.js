'use strict';

var express = require('express'),
    path = require('path'),
    config = require('./config'),
    passport = require('passport'),
    session = require('express-session'),
    mongoStore = require('connect-mongo')({session: session}),
    cookieParser = require('cookie-parser'),
    errorHandler = require('express-error-handler'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    multer  = require('multer'),
    compression = require('compression'),
    favicon = require('serve-favicon');


/**
 * Express configuration
 */
module.exports = function (app) {

    app.set('views', config.root+'/dist');
    app.set('view engine', 'hogan-express');
    app.engine('html', require('hogan-express'));

    app.use(multer({ dest: './uploads/'}));
    app.use(errorHandler());
    app.use(morgan('dev'));
    app.use(bodyParser());
    app.use(compression());
    app.use(methodOverride());
    app.use(cookieParser());
    app.use(favicon(config.root+'/app/favicon.ico'));


    app.use('/public', express.static(path.join(config.root, 'uploads'),{ maxAge:  3*86400000 }));
    app.use('/components', express.static(path.join(config.root, 'bower_components'),{ maxAge:  3*86400000 }));
    app.use('/assets', express.static(path.join(config.root,'dist/assets'),{ maxAge:  86400000 }));

    // Persist sessions with mongoStore
    app.use(session({
        secret: 'ThisISABanana',
        store: new mongoStore({
            url: config.mongo.uri,
            collection: 'sessions'
        })
    }));

    // Use passport session
    app.use(passport.initialize());
    app.use(passport.session());

};