'use strict';

module.exports = {
    env: 'production',
    mongo: {
        uri: process.env.MONGOLAB_URI ||
            process.env.MONGOHQ_URL ||
            'mongodb://localhost/lan'
    },
    url: 'http://beta.hringurinn.net',
    facebook: {
        appId: '1417743428473625',
        appSecret: '599b9ce47cfd70ab4da5a72ec41957d6',
        clientToken: 'c10e554c11faab03fa6208d91535397c'
    }
};