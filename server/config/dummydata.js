'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Team = mongoose.model('Team'),
    Game = mongoose.model('Game'),
    Settings = mongoose.model('Settings');
/**
 * Populate database with sample application data
 */

var self = this;

this.defaultSettings = new Settings({
    'registration': true,
    'closeRegistration': new Date('2014-04-01T17:07:18.239Z'),
    'openRegistration': new Date('2014-08-01T17:07:18.239Z'),
    'price': 3500,
    'capacity': 250
});

Settings.find({}, function (err, settings) {
    if (settings.length === 0) {
        self.defaultSettings.save()
    }
});

/*
 var user, game, team;

 // Clear old users, then add a default user
 user = new User({
 provider: 'local',
 nick: 'Test User',
 name : 'Snorri Örn Daníelsson',
 email: 'test@test.com',
 password: 'test',
 ssn: '2403902139',
 phone: '6638920'
 });

 // Clear old games and add a simple game
 game = new Game({
 name: 'LOL',
 fullName: 'League Of Legends',
 minNrOfPlayers: 4,
 description: 'A fun game to play with friends',
 rules: 'All out killing spree'
 });

 team = new Team({
 name: 'dummyTeam',
 game: game._id,
 members: [user._id],
 admin: user._id,
 created: {
 on: new Date()
 }
 });


 Game.find({}).remove(function () {
 Team.find({}).remove(function () {
 User.find({}).remove(function () {
 Game.find({}).remove();
 Team.find({}).remove();
 user.save(function (err, newUser) {
 if (err) console.log(err);
 game.save(function (err, newGame) {
 if (err) console.log(err);
 team.save(function (err, newTeam) {
 if (err) console.log(err);
 });
 })
 });
 });
 });
 });

 */