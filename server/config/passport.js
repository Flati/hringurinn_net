'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    FacebookTokenStrategy = require('passport-facebook-token').Strategy;

/**
 * Passport configuration
 */
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findOne({
        _id: id
    }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
        done(err, user);
    });
});

// add other strategies for more authentication flexibility
passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password' // this is the virtual field on the model
    },
    function (email, password, done) {
        User.findOne({
            email: email
        }, function (err, user) {
            if (err) return done(err);

            if (!user) {
                return done(null, false, {
                    message: 'This email is not registered.'
                });
            }
            if (!user.authenticate(password)) {
                return done(null, false, {
                    message: 'This password is not correct.'
                });
            }
            return done(null, user);
        });
    }
));

passport.use(new FacebookTokenStrategy({
        clientID: '1417743428473625',
        clientSecret: '599b9ce47cfd70ab4da5a72ec41957d6'
    },
    function (accessToken, refreshToken, profile, done) {
        User.findOne({ facebook: profile.id }, function (err, user) {
            if(err){
                console.log(err);
                done(null,err);
            }
            if (!err && user != null) {
                return done(null, user);
            } else {
                return done(null, {
                    message: 'User does not exist'
                });
            }
        });
    }
));

module.exports = passport;