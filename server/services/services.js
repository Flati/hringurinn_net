'use strict';

var $q = require('q'),
    request = require('request'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    Team = mongoose.model('Team'),
    Settings = mongoose.model('Settings'),
    Game = mongoose.model('Game'),
    moment = require('moment'),
    _ = require('lodash');

exports.downloadFacebookImage = function (userId) {
    var deferred = $q.defer();
    //Todo: clean up
    var filename = __dirname + '/../../uploads/' + userId + '.jpg';
    try {
        request('https://graph.facebook.com/' + userId + '/picture?width=500&height=500')
            .pipe(fs.createWriteStream(filename))
            .on('close', function () {
                deferred.resolve();
            })
            .on('error', function (err) {
                console.log(err);
                deferred.reject({type: 'failedToWriteImage', message: 'Failed to write image to server'});
            });
    } catch (e) {
        // Here you get the error when the file was not found,
        // but you also get any other error
        console.log('error ' + e);
        deferred.reject();
    }

    return deferred.promise;

};

exports.registerUserInTeamWithJoinCode = function (teamId, userId, joinCode) {
    var deferred = $q.defer();
    Team.findOne({ joinCode: joinCode }).populate('game').exec(function (err, team) {
        if (err) {
            deferred.reject();
        }
        else if (team) {
            // Add member to team and approve him
            team.members.push({
                approved: true,
                user: userId
            });
            team.save(function (err) {
                if (err) {
                    deferred.reject(err);
                }
                else {
                    deferred.resolve();
                }
            })
        }
    });
    return deferred.promise;
};

exports.removeFromTeam = function (userId) {

    var deferred = $q.defer();
    if(!userId){
        deferred.reject();
        return deferred.promise;
    }

    Team.findOne().or([
        { members: userId},
        { applicants: userId},
        { admin: userId }
    ]).exec(function (err, team) {
        if (err) {
            deferred.reject();
        }

        if (team) {
            if (team.admin.toString() === userId.toString()) {
                team.remove(function (err) {
                    if (err) return deferred.reject();
                    deferred.resolve();
                });
            }
            else {
                // This could be simplified
                var indexOfInMembers = _.findIndex(team.members, function (member) {
                    return member.toString() === userId.toString()
                });
                if (indexOfInMembers !== -1) {
                    team.members.splice(indexOfInMembers, 1);
                }
                var indexOfInApplicants = _.findIndex(team.applicants, function (member) {
                    return member.toString() === userId.toString()
                });
                if (indexOfInApplicants !== -1) {
                    team.applicants.splice(indexOfInApplicants, 1);
                }

                team.save(function (err) {
                    if (err) return deferred.reject();
                    deferred.resolve();
                });
            }
        }
        else {
            deferred.resolve();
        }
    });
    return deferred.promise;
};


var _settings = null;

exports.getSettings = function(){

        var deferred = $q.defer();
        if(_settings){
            deferred.resolve(_settings);
        }
        else {
            //TODO: Make this more reusable.
            Settings.findOne({}, 'registration closeRegistration', function (err, settings) {
                if (err) return {};
                _settings = settings.toObject();
                if(settings.closeRegistration){
                    var close = moment(settings.closeRegistration);
                    var now = moment();
                    _settings.register = close.diff(now) > 0;
                }
                else {
                    _settings.register = true;
                }
                deferred.resolve(_settings);
            });
        }
        return deferred.promise;
};