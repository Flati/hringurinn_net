'use strict';
var mailer = require('nodemailer'),
    $q = require('q'),
    logger = require('./../logger'),
    config = require('../config/config');

// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = mailer.createTransport("SMTP", {
    service: "mailgun",
    auth: {
        user: config.mail.user,
        pass: config.mail.password
    }
});


exports.sendMail = function (mailObj) {
    var deferred = $q.defer();

    // send mail with defined transport object
    smtpTransport.sendMail(mailObj, function (error, response) {
        if (error) {
            logger.log('info',  {
                action: 'Sending mail',
                email: mailObj.to
            });
            deferred.reject(error);
        } else {
            logger.log('error',  {
                action: 'Failed sending mail',
                email: mailObj.to
            });
            deferred.resolve();
        }

        smtpTransport.close(); // shut down the connection pool, no more messages
    });

    return deferred.promise;
};