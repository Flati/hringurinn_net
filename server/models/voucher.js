'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VoucherSchema = new Schema({
    token: String
});


module.exports = mongoose.model('Voucher', VoucherSchema);


