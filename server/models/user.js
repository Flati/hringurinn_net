'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto'),
    logger = require('./../logger');


var authTypes = ['github', 'twitter', 'facebook', 'google'];

/**
 * User Schema
 */
var UserSchema = new Schema({
    nick: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    email: String,
    role: {
        type: String,
        default: 'user'
    },
    hashedPassword: String,
    provider: String,
    salt: String,
    ssn: String,
    phone: {
        type: String
    },
    registered: { type: Date, default: Date.now },
    facebook: String,
    entry: {
        token: String,
        entryTime: Date,
        card: Number,
        cash: Number,
        parentSSN: String,
        parentPhone: String,
        comment: String
    },
    passwordReset: {
        resetTime: Date,
        code: String
    },
    extra: {}
});


/**
 * Virtuals
 */
UserSchema
    .virtual('password')
    .set(function (password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () {
        return this._password;
    });

// Basic info to identify the current authenticated user in the app
UserSchema
    .virtual('userInfo')
    .get(function () {
        return {
            'name': this.name,
            'role': this.role,
            'provider': this.provider
        };
    });

// Public profile information
UserSchema
    .virtual('profile')
    .get(function () {
        return {
            'name': this.name,
            'role': this.role
        };
    });

/**
 * Validations
 */

// Validate empty email
UserSchema
    .path('email')
    .validate(function (email) {
        // if you are authenticating by any of the oauth strategies, don't validate
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return email.length;
    }, 'Email cannot be blank');

// Validate name length
UserSchema
    .path('name')
    .validate(function (name) {
        if (name.length >= 3 && name.length <= 50) {
            return name.length
        }
        return false;
    }, 'Name must between 3 and 50 characters');


// Validate nick length
UserSchema
    .path('nick')
    .validate(function (nick) {
        if (nick.length >= 3 && nick.length <= 20) {
            return nick.length
        }
        return false;
    }, 'Nick must between 3 and 20 characters');


// Validate phone length
UserSchema
    .path('phone')
    .validate(function (phone) {
        if (phone.length >= 7) {
            return phone.length
        }
        return false;
    }, 'Phone number must more than 7 chars');


// Validate empty email
UserSchema
    .path('name')
    .validate(function (name) {
        return name.length;
    }, 'Name cannot be blank');


// Validate social security number
UserSchema
    .path('ssn')
    .validate(function (ssn) {
        if (ssn.length === 10) {
            var checkNr = [3, 2, 7, 6, 5, 4, 3, 2];
            var kt = ssn.split('').map(function (num) {
                return parseInt(num, 10);
            });
            var num = 0;
            for (var i = 0; i < 8; ++i) {
                num += kt[i] * checkNr[i];
            }
            var checkNum = (num % 11) === 0 ? (num % 11) : 11 - (num % 11);

            if (kt[8] === checkNum && (kt[9] === 9 || kt[9] === 0)) {
                return true;
            }
        }
        logger.log('warn',ssn);
        return false;

    }, 'SSN not valid');


// Validate empty password
UserSchema
    .path('hashedPassword')
    .validate(function (hashedPassword) {
        // if you are authenticating by any of the oauth strategies, don't validate
        if (authTypes.indexOf(this.provider) !== -1) return true;
        return hashedPassword.length;
    }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
    .path('email')
    .validate(function (value, respond) {
        var self = this;
        this.constructor.findOne({email: value}, function (err, user) {
            if (err) throw err;
            if (user) {
                if (self.id === user.id) return respond(true);
                return respond(false);
            }
            respond(true);
        });
    }, 'The specified email address is already in use.');

var validatePresenceOf = function (value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function (next) {
        if (!this.isNew) return next();
        if (this.facebook) {
            next();
        }
        else if (!validatePresenceOf(this.hashedPassword) && authTypes.indexOf(this.provider) === -1)
            next(new Error('Invalid password'));
        else
            next();
    });

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.hashedPassword;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function () {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function (password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
    }
};

module.exports = mongoose.model('User', UserSchema);