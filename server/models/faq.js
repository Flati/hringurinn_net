'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var FaqSchema = new Schema({
    question: String,
    answer: String
});



module.exports = mongoose.model('Faq', FaqSchema);


