'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var GameSchema = new Schema({
	name: String,
	fullName: String,
	description: String,
	rules: String,
    rulesMD: String,
	minNumberOfPlayers: Number,
    maxNumberOfPlayers: Number
});



module.exports = mongoose.model('Game', GameSchema);


