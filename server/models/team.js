'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uuid = require('node-uuid'),
    Game = mongoose.model('Game'),
    _ = require('lodash'),
    validate = require('mongoose-validator');


/**
 * Validators
 */

var nameValidator = [
    validate({
        validator: 'isLength',
        arguments: [3, 40],
        message: 'Name should be between 3 and 50 characters'
    })
];

var TeamSchema = new Schema({
    name: {
        type: String,
        validate: nameValidator,
        required: true
    },
    game: {
        type: Schema.Types.ObjectId,
        ref: 'Game'
    },
    members: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    applicants: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    created: {
        on: Date
    },
    visible: {type: Boolean, default: true},
    joinCode: 'String'
});

TeamSchema.pre('save', function (next) {
    if (this.joinCode) {
        next();
    }
    else {
        this.joinCode = uuid.v4();
        next();
    }
});

module.exports = mongoose.model('Team', TeamSchema);

