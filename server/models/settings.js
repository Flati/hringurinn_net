'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SettingsSchema = new Schema({
    closeRegistration: Date,
    openRegistration: Date,
    price: Number,
    capacity: Number
});


module.exports = mongoose.model('Settings', SettingsSchema);


