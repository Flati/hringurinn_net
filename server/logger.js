var winston = require('winston');
require('winston-loggly');


//
// Logging levels
//
var config = {
    levels: {
        info: 2,
        warn: 4,
        debug: 5,
        error: 6
    },
    colors: {
        info: 'green',
        warn: 'yellow',
        debug: 'blue',
        error: 'red'
    }
};

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            colorize: true
        }),
        new (winston.transports.Loggly)({
            subdomain: 'snorri',
            inputToken: 'cf8d5a36-33ac-4d6c-946d-3ae033916b1f',
            json: true
        })
    ],
    levels: config.levels,
    colors: config.colors
});

module.exports = logger;