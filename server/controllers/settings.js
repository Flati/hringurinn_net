var mongoose = require('mongoose'),
    Settings = mongoose.model('Settings'),
    User = mongoose.model('User');

exports.index = function (req, res, next) {
    Settings.findOne({}, function (err, settings) {
        if (err) return res.send(500);
        res.send(settings);
    });
};