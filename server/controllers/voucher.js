var mongoose = require('mongoose'),
    Voucher = mongoose.model('Voucher'),
    fs = require('fs'),
    readline = require('readline');

exports.importVouchers = function (req, res) {
    var rd = readline.createInterface({
        input: fs.createReadStream(req.files.vouchers.path),
        output: process.stdout,
        terminal: false
    });
    var vouchers = [];

    rd.on('line', function (line) {
        vouchers.push({ token: line });
    });

    rd.on('close', function () {
        Voucher.create(vouchers, function (err) {
            if (err) {
                res.send(500);
            }
            fs.unlink(req.files.vouchers.path, function (err) {
                if (err) {
                    conosle.log('Failed to delete tmp file');
                }
                console.log('Deleted tmp file');
            });
            res.send(200);
        })
    });
};


exports.getAllVouchers = function (req, res) {
    Voucher.find({}, function (err, vouchers) {
        if (err) return res.send(500);
        return res.send(vouchers);
    })
};

exports.getSingleVoucher = function (req, res) {
    Voucher.findOne({}, function (err, voucher) {
        var voucherStr = voucher
        voucher.remove(function (err) {
            if (err) return res.send(500);
            res.send(200, voucher);
        });
    })
};