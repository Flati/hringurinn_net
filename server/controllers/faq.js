'use strict';

var mongoose = require('mongoose'),
    Faq = mongoose.model('Faq');


exports.index = function (req, res, next) {
    Faq.find({}, function (err, faq) {
        if (err) return next(err);
        if (!faq) return res.send(404);

        return res.send(faq);
    })
};

exports.create = function (req, res, next) {
    var faq = new Faq({
        question: req.body.question,
        answer: req.body.answer
    });

    faq.save(function (err) {
        if (err) return res.send(500);
        res.send(200);
    })
};

exports.remove = function(req, res, next){
    Faq.findByIdAndRemove(req.params.id,function(err){
        if(err) return res.send(500);
        res.send(200);
    })
};