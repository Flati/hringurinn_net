'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Team = mongoose.model('Team'),
    request = require('request'),
    $q = require('q'),
    services = require('../services/services'),
    _ = require('lodash'),
    mail = require('../services/mail'),
    uuid = require('node-uuid'),
    config = require('./../config/config'),
    Voucher = mongoose.model('Voucher'),
    randomstring = require('just.randomstring'),
    logger = require('./../logger'),
    moment = require('moment');

/**
 * Create user
 */
exports.create = function (req, res, next) {
    logger.log('info', {
        action: 'Creating user',
        ssn: req.body.ssn,
        name: req.body.name
    });

    var deferred = $q.defer();
    var user = {};

    if (req.body.code) {
        console.log(JSON.stringify(req.body));
        // We register a facebook user
        request('https://graph.facebook.com/me?access_token=' + req.body.token, function (graphError, graphRes) {
            if (!graphError && graphRes.statusCode == 200) {
                //console.log(JSON.stringify(graphRes));
                var FBObj = JSON.parse(graphRes.body);
                user.name = FBObj.name;
                user.email = FBObj.email;
                user.facebook = FBObj.id;
                user.nick = req.body.nick;
                user.ssn = req.body.ssn;
                user.phone = req.body.phone;
                deferred.resolve();
                /*
                 services.downloadFacebookImage(user.facebook)
                 .then(function () {
                 deferred.resolve();
                 }, function () {
                 deferred.reject();
                 });
                 */
            }
            else {
                logger.log('error', {
                    action: 'Error creating user',
                    ssn: req.body.ssn,
                    name: req.body.name,
                    message: 'Graph error',
                    graphError: graphError,
                    graphRes: graphRes
                });
                deferred.reject();
            }
        })
    }
    else {

        user.nick = req.body.nick;
        user.name = req.body.name;
        user.email = req.body.email;
        user.ssn = req.body.ssn;
        user.phone = req.body.phone;
        user.password = req.body.password;

        deferred.resolve();

    }

    deferred.promise.then(function () {
        var newUser = new User(user);
        if (user.facebook) {
            newUser.provider = 'facebook'
        }
        else {
            newUser.provider = 'local';
        }

        newUser.save(function (err) {
            if (err) return res.json(400, err);


            req.logIn(newUser, function (err) {
                if (err) return next(err);

                return res.json(req.user.userInfo);
            });
        });
    }, function () {
        logger.log('error', {
            action: 'Error Creating user',
            ssn: req.body.ssn,
            name: req.body.name
        });
        res.json(400, 'Error creating user');
    });

};

exports.getAllUsers = function (req, res, next) {
    User.find({}, function (err, users) {
        if (err) return next(err);
        if (!users) return res.send(404);

        res.send(users);
    });
};

exports.sendResetPasswordCode = function (req, res, next) {

    logger.log('info', {
        action: 'Sending password reset',
        email: req.body.email
    });

    var code = uuid.v4();

    var mailOptions = {
        from: "HRingurinn <no-reply@hringurinn.net>", // sender address
        subject: "Gleymt lykilorð ", // Subject line
        text: config.url + '/#/user/forgotPassword/' + code // plaintext body
    };


    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) return next(err);
        if (!user) return res.send(404);

        user.passwordReset = {
            resetTime: Date.now(),
            code: code
        };

        user.save();

        mailOptions.to = user.email;

        mail.sendMail(mailOptions)
            .then(function () {
                res.send(200);
            }, function () {
                res.send(500);
            });
    })
};

exports.resetPassword = function (req, res, next) {
    logger.log('info', {
        action: 'Password reset',
        code: req.body.code
    });

    User.findOne({'passwordReset.code': req.body.code }, function (err, user) {
        if (err) return next(err);
        if (!user) return res.send(404);
        // TODO: check time
        if (typeof user.passwordReset.code !== 'undefined') {
            if (req.body.code === user.passwordReset.code) {
                user.password = req.body.password;
                user.passwordReset = null;
                user.save(function () {
                    logger.log('info', {
                        action: 'Password reset success',
                        user: user._id.toString()
                    });
                    res.send(200);
                });
            }
            else {
                logger.log('error', {
                    action: 'Password reset failed',
                    user: user._id.toString(),
                    code: req.body.code
                });

                res.send(400);
            }
        }
        else {
            res.send(404);
        }
    });
};

/**
 *  Get profile of specified user
 */
exports.show = function (req, res, next) {
    var userId = req.params.id;

    User.findById(userId, function (err, user) {
        if (err) return next(err);
        if (!user) return res.send(404);

        //if()

        res.send({ profile: user.profile });
    });
};

/**
 * Check if user exists
 */

exports.exists = function (req, res, next) {

    var query = {};

    if (req.params.email) {
        query.email = req.params.email;
    }
    else if (req.params.ssn) {
        query.ssn = req.params.ssn;
    }
    else {
        return res.send(400);
    }

    User.findOne(query).exec(function (err, user) {
        if (user) {
            return res.json({exists: true});
        }

        res.json({exists: false});

    });

};

/**
 * Change password
 */
exports.changePassword = function (req, res, next) {
    var userId = req.user._id;
    var currentPass = String(req.body.currentPassword);
    var newPass = String(req.body.newPassword);

    logger.log('info', {
        action: 'User change password',
        user: req.user.id
    });

    User.findById(userId, function (err, user) {
        if (user.authenticate(currentPass)) {
            user.password = newPass;
            user.save(function (err) {
                if (err) return res.send(400);

                res.send(200);
            });
        } else {
            res.send(403);
        }
    });
};

/**
 * Get current user
 */
exports.me = function (req, res) {
    var user = null;

    Team.findOne().or([
        { members: req.user._id },
        { admin: req.user._id },
        { applicants: req.user._id }
    ])
        .exec(function (err, team) {
            if (err) res.send(500);
            else if (team) {
                // Build user object
                user = req.user.toObject();
                team = team.toObject();
                user.team = {
                    _id: team._id,
                    isAdmin: user._id.toString() === team.admin.toString(),
                    game: team.game,
                    name: team.name,
                    isApplicant: !!_.find(team.applicants, function (applicant) {
                        return applicant.toString() === user._id.toString();
                    })
                };

                //Remove properties, there is a better place for this?
                delete user.passwordReset;

                res.send(user);

            }
            else {

                user = req.user.toObject();
                delete user.passwordReset;

                res.json(user || null);
            }
        });
};


exports.entry = function (req, res) {
    var deferred = $q.defer();
    var user = null;
    if (req.params.userId == 'new') {
        user = new User();
        user.name = req.body.name;
        user.ssn = req.body.ssn;
        user.email = req.body.email;
        user.phone = req.body.phone;
        user.provider = 'local';
        user.password = randomstring(6);
        user.save(function (err) {
            if (err) return res.send(500);
            deferred.resolve(user)
        })
    }
    else {
        User.findById(req.body._id, function (err, reqUser) {
            if (err) return res.send(500);
            user = reqUser;
            deferred.resolve(user);
        });
    }

    deferred.promise.then(function () {
        Voucher.findOne({}, function (err, voucher) {
            if (!voucher && !voucher.token) {
                res.send(404, 'No vouchers');
            }
            req.body.entry.token = voucher.token;
            req.body.entry.entryTime = new Date();
            user.entry = req.body.entry;
            voucher.remove(function (err) {
                if (err) {
                    res.send(500)
                }
                user.save(function (err) {
                    if (err) {

                        res.send(500)
                    }
                    else {
                        var response = {
                            token: voucher.token
                        };

                        if (user.password) {
                            response.password = user.password;
                        }
                        res.send(200, response);
                    }
                });
            });

        });
    })
};

exports.linkToFacebook = function (req, res) {
    logger.log('info', {
        action: 'Linking to facebook',
        user: req.body.id
    });
    // Check if there is a user already associated with this userID
    User.findOne({facebook: req.body.id}, function (err, user) {
        if (user) {
            res.send(409);
        }
        else {
            User.findById(req.user.id, function (err, user) {
                user.facebook = req.body.id;
                user.save(function (err) {
                    if (err) return res.send(400);
                    /*
                     services.downloadFacebookImage(req.body.id)
                     .then(function () {

                     }, function () {
                     req.send(500);
                     });
                     */
                    logger.log('info', {
                        action: 'Success linking to facebook',
                        user: req.user.id,
                        fbId: req.body.id
                    });

                    res.send(200, req.body.id);
                });
            });
        }
    });
};

exports.deleteUser = function (req, res) {
    if (req.params.id === 'me') {
        var id = req.user._id.toString();
        logger.log('info', {
            action: 'Deleting user',
            user: req.user.id
        });
    }
    else {
        if (req.user.role === 'admin') {
            id = req.params.id;
        }
        else {
            return res.send(401);
        }
    }

    services.removeFromTeam(id).then(function () {
        User.findByIdAndRemove(id, function (err) {
            if (err) return res.send(500);
            res.send(200);
        });

    }, function () {
        res.send(500);
    })

};

exports.joinTeam = function (req, res) {
    logger.log('info', {
        action: 'Joining team',
        user: req.params.userId,
        userID: req.user.id,
        team: req.params.teamId
    });

    Team.findById(req.params.teamId)
        .populate('game').exec(function (err, team) {
            if (err) return res.json(404, { type: 'teamNotFound', message: 'Team not found' });

            var userId = undefined;
            // Is this the current user asking?
            if (req.params.userId === 'me') {
                userId = req.user._id;
            }
            else if (req.user.role === 'admin') {
                userId = req.params.userId;
            }
            else {
                res.json(401, { type: 'notAllowed', message: 'You do not have permission to do this action' });
            }
            // Check if member is already in some team
            services.removeFromTeam(userId).then(function () {
                if (!team.applicants) {
                    team.applicants = [];
                }
                team.applicants.push(userId);

                team.save(function (err) {
                    if (err) return res.json(500, {type: 'failedToSave', message: 'Failed to save user to team'});
                    res.json(200);
                })
            }, function () {
                res.send(500);
            });

        })
};


exports.approveTeamMember = function (req, res, next) {

    if (!req.params.teamId || !req.params.userId) {
        return json(400, {action: 'malformedRequest', message: 'teamId and userId must be defined'});
    }

    logger.log('info', {
        action: 'Approved in team',
        user: req.params.userId,
        userID: req.user.id,
        team: req.params.teamId
    });


    Team.findById(req.params.teamId, function (err, team) {
        if (err) return res.json(400, {action: 'notFound', message: 'Team not found'});

        if ((team.admin.toString() === req.user._id.toString()) || req.user.role === 'admin') {
            var indexOfApplicant = _.findIndex(team.applicants, function (member) {
                return member.toString() === req.params.userId;
            });

            team.members.push(team.applicants.splice(indexOfApplicant, 1));

            team.save(function (err) {
                if (err) return res.json(500, {action: 'saveFailed', message: 'Failed to modify team'});
                return res.json(200);
            });
        }
        else {
            res.json(401, { type: 'notAllowed', message: 'You do not have permission to make this request' });
        }

    });
};


exports.leaveTeam = function (req, res, next) {
    // TODO: Refactor
    if (!req.params.teamId || !req.params.userId) {
        return json(400, {action: 'malformedRequest', message: 'teamId and userId must be defined'});
    }

    logger.log('info', {
        action: 'Leaving team',
        user: req.params.userId,
        userID: req.user.id,
        team: req.params.teamId
    });

    Team.findById(req.params.teamId, function (err, team) {
        if (err) return res.json(400, {action: 'notFound', message: 'Team not found'});

        if ((team.admin.toString() === req.user._id.toString())
            || req.user.role === 'admin'
            || req.user._id.toString() === req.params.userId) {


            services.removeFromTeam(req.params.userId).then(function () {
                logger.log('info', {
                    action: 'Success in leaving team',
                    user: req.params.userId,
                    userID: req.user.id,
                    team: req.params.teamId
                });

                res.json(200);
            }, function () {
                res.json(500, {action: 'saveFailed', message: 'Failed to modify team'});
            });

        }
        else {
            res.send(401);
        }
    });
};

exports.edit = function (req, res, next) {

    if (!req.params.id) return res.send(400);

    User.findById(req.params.id, function (err, user) {
        if (!user || err) return res.send(500);

        user.name = req.body.name ? req.body.name : user.name;
        user.email = req.body.email ? req.body.email : user.email;
        user.phone = req.body.phone ? req.body.phone : user.phone;
        user.nick = req.body.nick ? req.body.nick : user.nick;

        user.entry = req.body.entry;
        //Change permission
        user.role = req.body.role ? req.body.role : user.role;

        user.save(function (err) {
            if (err) return res.send(500);
            res.send(200);
        })

    })
};