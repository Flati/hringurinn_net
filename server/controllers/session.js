'use strict';

var mongoose = require('mongoose'),
    passport = require('passport');

/**
 * Logout
 */
exports.logout = function (req, res) {
    req.logout();
    res.send(200);
};

/**
 * Login
 */
exports.login = function (req, res, next) {
    if (req.body.access_token) {
        passport.authenticate('facebook-token', function (err, user, info) {
            var error = err || info;
            if (error) return res.json(401, error);

            req.logIn(user, function (err) {

                if (err) return res.send(401);
                res.json(req.user.userInfo);
            });
        })(req, res, next);
    }
    else {
        passport.authenticate('local', function (err, user, info) {
            var error = err || info;
            if (error) return res.json(401, error);

            req.logIn(user, function (err) {

                if (err) return res.send(err);
                res.json(req.user.userInfo);
            });
        })(req, res, next);
    }

};