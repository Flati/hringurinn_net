'use strict';

var mongoose = require('mongoose'),
    Game = mongoose.model('Game'),
    passport = require('passport'),
    hl = require('highlight.js');


exports.index = function (req, res, next) {
    Game.find({}, function (err, games) {
        if (err) return next(err);
        if (!games) return res.send(404);

        return res.send(games);
    })
};

exports.gameById = function (req, res, next) {
    Game.findById(req.params.id, function (err, game) {
        if (err) return next(err);
        if (!game) return res.send(400);

        return res.send(game);
    })
};

exports.create = function (req, res, next) {
    var newGame = new Game(req.body);
    newGame.save(function (err) {
        if (err) return res.json(400, err);

        return res.json(200, {message: 'success'});
    })
};

exports.update = function(req, res, next){
    Game.findById(req.params.id, function (err, game) {
        if (err) return next(err);
        if (!game) return res.send(400);

        game.rulesMD = req.body.rulesMD;
        game.rules = hl.highlight('markdown',req.body.rulesMD);
        game.save(function(err){
            if(err) return res.send(500);
            return res.send(game);
        })

    })
};

exports.preview = function(req, res, next){
    res.send(200,hl.highlight('markdown',req.body.foo));
};