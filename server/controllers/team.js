'use strict';

var mongoose = require('mongoose'),
    Team = mongoose.model('Team'),
    User = mongoose.model('User'),
    logger = require('./../logger'),
    _ = require('lodash'),
    services = require('../services/services'),
    $q = require('q');


exports.index = function (req, res, next) {
    Team.find({})
        .populate('game')
        .populate('admin','-hashedPassword -salt')
        .populate('members','-hashedPassword -salt')
        .populate('applicants','-hashedPassword -salt')
        .exec(function (err, teams) {
            if (err) return next(err);
            if (!teams) return res.send(404);

            var nTeams = [];
            _.forEach(teams, function (team) {
                var tmpTeam = team.toObject();


                // Is Team full?
                tmpTeam.joinable = tmpTeam.members.length >= tmpTeam.game.maxNumberOfPlayers;

                if (req.user.role === 'admin') {
                    // Do something for admins?
                }
                else {
                    delete tmpTeam.members;
                    delete tmpTeam.applicants;
                    delete tmpTeam.admin;
                    delete tmpTeam.joinCode;
                }


                nTeams.push(tmpTeam);
            });

            return res.send(nTeams)
        });
};

exports.teamById = function (req, res, next) {
    // Check if valid
    if (!/^[0-9a-fA-F]{24}$/.test(req.params.id)) {
        return res.send(500);
    }
    Team.findById(req.params.id)
        .populate('game')
        .populate('members', 'name nick email phone')
        .populate('applicants', 'name nick email phone')
        .exec(function (err, team) {
            if (err) return next(err);
            if (!team) return res.send(404);
            console.log(req.params.id);

            // Check if user is in team
            var foundInTeam = _.find(team.members, function (member) {
                return member._id.toString() === req.user._id.toString();
            });
            var isAdmin = team.admin.toString() === req.user._id.toString();
            if (isAdmin || foundInTeam) {
                team.joinable = false;
                team.joinable = team.members.length >= team.game.maxNumberOfPlayers;

                team = team.toObject();
                if (!isAdmin) {
                    delete team.joinCode;
                    delete team.applicants;
                }


                return res.send(team);
            }

            res.send(401);
        });
};

exports.create = function (req, res, next) {
    // Check if user is already in other team


    logger.log('info', {
        action: 'Creating team',
        user: req.user.id,
        name: req.body.name,
        game: req.body.game._id
    });

    services.removeFromTeam(req.user._id).then(function () {

        var newTeam = new Team({
            name: req.body.name,
            game: req.body.game._id,
            admin: req.user._id,
            members: [req.user._id]
        });

        newTeam.save(function (err) {
            if (err) return res.json(400, err);

            return res.json(200, newTeam);
        })
    }, function () {
        res.send(500);
    });


};


exports.remove = function (req, res, next) {
    var teamId = req.params.id;


    logger.log('info', {
        action: 'Removing team',
        userID: req.user.id,
        team: req.params.id
    });

    if (req.params.id) {
        Team.findOne({ _id: req.params.id}, function (err, team) {
            if (err) return next(err);
            if (!team) return res.send(404);
            // User is not admin of the team
            if ((team.admin.toString() === req.user._id.toString()) || req.user.role === 'admin') {
                // If the user is admin of the team we can delete
                team.remove(function (err) {
                    if (err) return res.send(500);
                    return res.send(200);
                });
            }
            else {
                res.send(401);
            }
        });
    }
    else {
        return res.send(400);
    }

};

exports.edit = function (req, res, next) {
    if (req.params.teamId) {
        var teamId = req.params.teamId;
    }

    Team.findById(teamId, function (err, team) {
        if (err) return res.send(500);
        if(!team) return res.send(404);
        // Check if user has permission to do the change
        if (req.user.id === team.admin.toString() || req.user.role === 'admin') {
            // We can only change the name for now.
            team.name = req.body.name ? req.body.name : team.name;
            team.save(function(err){
                if(err) return res.send(500);
                return res.send(200);
            })
        }
        else {
            return res.send(401);
        }
    })

};