'use strict';

var mongoose = require('mongoose'),
    _ = require('lodash'),
    User = mongoose.model('User'),
    Team = mongoose.model('Team');


exports.index = function (req, res, next) {
    User.find({}, function (err, users) {
        Team.find({}, function(tErr, teams){
            if (err) return res.send(500);

            var response = {
                numberOfUsers: 0,
                numberOfOtherRoles: 0,
                numberOfUsersInTeam: 0,
                numberOfUsersNotInTeam: 0
            };

            _.forEach(users, function(user){
                if(user.role === 'user'){
                    response.numberOfUsers++;
                }
                else {
                    response.numberOfOtherRoles++;
                }
            });

            _.forEach(teams, function(team){
                response.numberOfUsersInTeam += team.members.length;
            });

            response.numberOfUsersNotInTeam = response.numberOfUsers - response.numberOfUsersInTeam;


            res.json(response);
        })

    });
};