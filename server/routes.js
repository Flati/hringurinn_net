'use strict';

var api = require('./controllers/api'),
    index = require('./controllers'),
    users = require('./controllers/users'),
    session = require('./controllers/session'),
    team = require('./controllers/team'),
    game = require('./controllers/game'),
    voucher = require('./controllers/voucher'),
    settings = require('./controllers/settings'),
    stats = require('./controllers/stats'),
    faq = require('./controllers/faq'),
    path = require('path'),
    services = require('./services/services'),
    $q = require('q'),
    config = require('./config/config');

var middleware = require('./middleware');


var basicAuth = require('basic-auth');

var auth = function (req, res, next) {
    if (process.env.NODE_ENV == 'production') {
        next();
    }
    else {
        var user = basicAuth(req);

        if (!user || !user.name || !user.pass) {
            return unauthorized(res);
        }

        if (user.name === 'glaum' && user.pass === 'bar') {
            return next();
        } else {
            return unauthorized(res);
        }
    }
    function unauthorized(res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(401);
    }

};


/**
 * Application routes
 */
module.exports = function (app) {
    // User
    app.post('/api/users', middleware.checkIfRegistrationClosed, users.create);
    app.get('/api/users', middleware.auth, middleware.roles(['admin']), users.getAllUsers);
    // Users Password

    app.put('/api/users/password', middleware.auth, users.changePassword);
    app.post('/api/users/password/reset', users.sendResetPasswordCode);
    app.put('/api/users/password/reset', users.resetPassword);
    // Get individual usero
    app.get('/api/users/me', middleware.auth, users.me);
    app.get('/api/users/:id', middleware.auth, middleware.roles(['admin']), users.show);
    app.put('/api/users/:id', middleware.auth, middleware.roles(['admin']), users.edit);
    app.delete('/api/users/:id', middleware.auth, users.deleteUser);
    // Misc
    app.get('/api/users/exists/:ssn?/:email?', users.exists);
    // User / Team
    app.post('/api/users/:userId/team/:teamId', middleware.auth, users.joinTeam);
    app.delete('/api/users/:userId/team/:teamId', middleware.auth, users.leaveTeam);
    app.put('/api/users/:userId/team/:teamId', middleware.auth, users.approveTeamMember);
    // User / check in
    app.post('/api/users/:userId/entry', middleware.auth, users.entry);

    //Linkers
    app.post('/api/user/link/facebook', middleware.auth, users.linkToFacebook);

    //Settings
    app.get('/api/settings', middleware.auth, settings.index);

    // Count of users etc.
    app.get('/api/stats', middleware.auth, middleware.roles(['admin']), stats.index);

    //Faq
    app.get('/api/faq', faq.index);
    app.post('/api/faq', middleware.auth, middleware.roles(['admin']), faq.create);
    app.delete('/api/faq/:id', middleware.auth, middleware.roles(['admin']), faq.remove);

    // Session
    app.post('/api/session', session.login);
    app.delete('/api/session', session.logout);

    // Team
    app.get('/api/team', middleware.auth, team.index);
    app.get('/api/team/:id', middleware.auth, team.teamById);
    app.post('/api/team', middleware.auth, team.create);
    app.put('/api/team/:teamId', middleware.auth, team.edit);
    app.delete('/api/team/:id', middleware.auth, team.remove);

    // Game
    app.get('/api/game', game.index);
    app.get('/api/game/:id', game.gameById);
    app.put('/api/game/:id', game.update);
    app.post('/api/game/preview', game.preview);
    app.post('/api/game', middleware.auth, middleware.roles(['admin']), game.create);


    app.post('/api/voucher', middleware.auth, middleware.roles(['admin']), voucher.importVouchers);
    app.get('/api/voucher', middleware.auth, middleware.roles(['admin']), voucher.getAllVouchers);
    app.get('/api/voucher/retrieve', middleware.auth, middleware.roles(['admin']), voucher.getSingleVoucher);

    // All undefined api routes should return a 404
    app.get('/api/*', function (req, res) {
        res.send(404);
    });

    app.all('/assets/*', function (req, res) {
        res.send(404);
    });

    app.all('/components/*', function (req, res) {
        res.send(404);
    });

    app.all('/*', function (req, res) {
        // Set up some config in the html
        services.getSettings().then(function(settings){
            res.render('index.html', {'config': JSON.stringify(settings)});
        });
        //res.sendfile(path.join(config.root, 'dist/index.html'));
    });
};