angular.module('lan.main', [
    'ui.router',
    'ui.bootstrap',
    'lan.common',
    'lan.user',
    'lan.game',
    'lan.dashboard',
    'lan.admin',
    'lan.team',
    'lan.faq',
    'lan.static',
    'lan.partners',
    'ngAnimate',
    'hc.marked'
])
    .config(function ($urlRouterProvider, $locationProvider, toastrServiceProvider, $httpProvider, markedProvider) {
        /**
         * App configuration
         */

        toastrServiceProvider.options({
            closeButton: true,
            "positionClass": "toast-bottom-full-width"
        });

        markedProvider.setOptions({gfm: true});


        // Activate interceptor
        $httpProvider.interceptors.push('httpInterceptor');

        // Default route
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    })

    .run(function (authService) {
        authService.checkIfAuthenticated();
    })
    .factory('httpInterceptor', function ($q, $injector) {
        return {
            'responseError': function (res) {
                if (res.status === 401) {
                    if(res.config.url !== '/api/users/me'){
                        $injector.invoke(function ($state) {
                            $state.go('user.login');
                        });
                    }
                }
                return $q.reject(res);
            }
        };
    })
    .constant('config',{
        url: 'http://hringurinn.net'
        //url: 'http://localhost:4000'
    });