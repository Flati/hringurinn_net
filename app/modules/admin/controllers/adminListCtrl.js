angular.module('lan.admin').controller('adminListCtrl', function ($scope, teamService, $modal) {

    function getAll() {
        teamService.getAll().then(function (teams) {
            $scope.teams = teams;
        });
    }

    getAll();

    $scope.edit = function (team) {
        var modalInstance = $modal.open({
            templateUrl: '/assets/modules/admin/partials/adminTeamEdit.html',
            controller: 'adminTeamEditModelCtrl',
            resolve: {
                team: function () {
                    return team;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response === 'refresh') {
                getAll();
            }
        }, function () {
            // Refresh?
        });
    };

    $scope.gameClass = function (game) {
        if (game === 'Hearthstone') {
            return 'label-default'
        }
        else if (game === 'LOL') {
            return 'label-success'
        }
        else if (game === 'DotA 2') {
            return 'label-warning'
        }
        else if (game === 'CS:GO') {
            return 'label-danger'
        }
    }

});


angular.module('lan.admin').controller('adminTeamEditModelCtrl', function ($scope, $modalInstance, teamService, team, gameService, $http, toastrService) {

    $scope.team = angular.copy(team);

    gameService.getAll().then(function (games) {
        $scope.games = games;
    });

    $scope.saveTeam = function (t) {
        $http.put('/api/team/' + t._id, { name: t.name }).success(function () {
            toastrService.success('Tókst að uppfæra lið');
            team.name = t.name;
            $modalInstance.close();
        }, function () {
            toastrService.error('Mistókt að uppfæra lið')
        });
    };

    $scope.removeMember = function (member) {
        var response = null;
        if (member._id === team.admin) {
            response = confirm("Ef þú eyðir stjórnanda liðsins þá hendiru sjálfu liðinu, ertu viss?");
        }
        else {
            response = true;
        }

        if (response) {
            _removeMember(member);
        }

    };


    function _removeMember(member) {
        return $http.delete('/api/users/' + member._id + '/team/' + team._id).success(function () {

            _.remove($scope.team.applicants, { _id: member._id });
            team.applicants = $scope.team.applicants;
            _.remove($scope.team.members, { _id: member._id });
            team.members = $scope.team.members;

            toastrService.success('Tókst að fjarlægja ' + member.name + ' úr liðinu');
            if (team.admin === member._id) {
                $modalInstance.close('refresh');
            }
        }, function () {
            toastrService.error('Mistókst að fjarlægja ' + member.name + ' úr liðinu')
        });
    }


    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }
});