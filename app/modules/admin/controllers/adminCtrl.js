angular.module('lan.admin').controller('adminCtrl', function ($scope, adminService, $http) {

    $scope.model = {};
    adminService.getSettings().then(function (settings) {
        $scope.model = _.extend($scope.model, settings);
    });

    $http.get('/api/stats').success(function (stats) {
        $scope.model = _.extend($scope.model, stats);
    });

});