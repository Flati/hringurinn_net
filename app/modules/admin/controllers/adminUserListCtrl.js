angular.module('lan.admin').controller('adminUserListCtrl', function ($scope, userService, $modal) {

    $scope.filterRoles = {
        admin: true,
        user: true,
        staff: true
    };

    function getAll() {
        userService.getAll().then(function (users) {
            $scope.users = users;
        });
    }

    getAll();

    $scope.edit = function (user) {
        var modalInstance = $modal.open({
            templateUrl: '/assets/modules/admin/partials/userEdit.html',
            controller: 'userEditModelCtrl',
            resolve: {
                user: function () {
                    return user;
                }
            }
        });

        modalInstance.result.then(function (response) {
            if (response === 'refresh') {
                getAll();
            }
        });
    };

}).controller('userEditModelCtrl', function ($scope, $modalInstance, $http, toastrService, user) {

    $scope.user = angular.copy(user);

    $scope.saveUser = function(u){
        $http.put('/api/users/'+u._id , u).success(function(){
            toastrService.success('Tókst að breyta notanda');
            user = $scope.user;
            $modalInstance.close('refresh');
        }, function(){
            toastrService.error('Mistókst að breyta notanda');
        })
    };

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
});
