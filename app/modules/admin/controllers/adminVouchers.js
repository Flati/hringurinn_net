angular.module('lan.admin').controller('adminVoucherCtrl',
    function ($scope, $http, toastrService) {
        function getVouchers() {
            $http.get('/api/voucher').success(function (vouchers) {
                $scope.vouchers = vouchers;
            });
        }

        getVouchers();

        $scope.clearVouchers = function () {
            var choice = confirm("Ertu viss?");
            if (choice) {
                $http.delete('/api/voucher')
                    .success(function () {
                        toastrService.success('Tókst að eyða voucherum')
                    }, function () {
                        toastrService.error('Mistókst að eyða voucherum');
                    });
            }
        };

        $scope.sendVouchers = function () {
            var formData = new FormData();

            formData.append('vouchers', $scope.file);

            $http({
                method: 'POST',
                url: '/api/voucher',
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity,
                data: formData
            })
                .success(function () {
                    getVouchers();
                }, function () {
                    toastrService.error('Mistókst að uppfæra vouchera');
                })
        };
        $scope.singleVouchers = [];
        $scope.getSingleVoucher = function(){
            $http.get('/api/voucher/retrieve').success(function(res){
                getVouchers();
                $scope.singleVouchers.push(res);
            })
        };

    });