angular.module('lan.admin').controller('adminDashboardCtrl', function ($scope, $modal, $http) {

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
});
