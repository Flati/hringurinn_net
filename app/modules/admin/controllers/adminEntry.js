angular.module('lan.admin').controller('adminEntryCtrl',
    function ($scope, $http, toastrService) {

        $scope.isNewUser = false;
        $scope.response = false;

        $scope.userSelected = function (user) {
            $scope.isNewUser = false;
            $scope.selectedUser = user;
        };

        $scope.newUser = function () {
            $scope.isNewUser = true;
            $scope.selectedUser = undefined;
        };

        $scope.finish = function () {
            if ($scope.isNewUser) {
                $scope.selectedUser._id = 'new';
            }

            $http.post('/api/users/' + $scope.selectedUser._id + '/entry', $scope.selectedUser)
                .success(function (res) {
                    $scope.response = res;
                }).error(function (res) {
                    toastrService.error('Villa við innritun');
                });
        };

        $scope.print = function () {
            window.print();
        };

        $scope.clear = function () {
            $scope.isNewUser = false;
            $scope.selectedUser = undefined;
            $scope.response = undefined;
        };

        $scope.is18 = function (ssn) {
            if (ssn && ssn.length === 10) {
                var century = ssn[9] > 0 ? 19 : 20;
                var birthday = moment().years(century +  ssn.substring(4, 6)).months(ssn.substring(2, 4)).days(ssn.substring(0, 2));
                var years = moment().diff(birthday, 'years');
                console.log(years);
                return years < 18;
            }
        };

        $scope.rePrint = function () {
            $scope.response = $scope.selectedUser;
        };
    });