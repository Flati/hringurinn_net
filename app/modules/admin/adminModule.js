angular.module('lan.admin',[])
	.config(function($stateProvider){
		$stateProvider
			.state('admin',{
				url: '/admin',
				template: '<ui-view></ui-view>',
				controller: 'adminCtrl',
				abstract: true
			})
            .state('admin.dashboard', {
                url: '/',
                templateUrl: '/assets/modules/admin/partials/adminDashboard.html',
                controller: 'adminDashboardCtrl'
            })
			.state('admin.list',{
				url: '/list',
				templateUrl: '/assets/modules/admin/partials/list.html',
				controller: 'adminListCtrl'
			})
            .state('admin.vouchers',{
                url: '/vouchers',
                templateUrl: '/assets/modules/admin/partials/vouchers.html',
                controller: 'adminVoucherCtrl'
            })
            .state('admin.entry',{
                url: '/entry',
                templateUrl: '/assets/modules/admin/partials/entry.html',
                controller: 'adminEntryCtrl'
            })
            .state('admin.users',{
                url: '/users',
                templateUrl: '/assets/modules/admin/partials/adminUserList.html',
                controller: 'adminUserListCtrl'
            });
	});