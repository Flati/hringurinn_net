angular.module('lan.admin').filter('roles', function() {
    return function(users, roles) {
        return _.filter(users, function(user){
            for(var role in roles){
                if(roles.hasOwnProperty(role)){
                    if(roles[role]){
                        if(role === user.role){
                            return true;
                        }
                    }
                }
            }
        })
    };
});