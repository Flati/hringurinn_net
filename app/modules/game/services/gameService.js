angular.module('lan.game').factory('gameService', function ($http) {


    function getAll() {
        return $http.get('/api/game')
            .then(function (res) {
                return res.data;
            });
    }

    function getById(id) {
        return $http.get('/api/game/' + id)
            .then(function (res) {
                return res.data;
            });
    }

    return {
        getAll: getAll,
        getById: getById
    }
});