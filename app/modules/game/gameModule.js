angular.module('lan.game',[])
	.config(function($stateProvider){
		$stateProvider
            .state('game',{
                url: '/game',
                template: '<ui-view></ui-view>',
                abstract: true
            })
			.state('game.view',{
				url: '/:id',
				templateUrl: '/assets/modules/game/partials/view.html',
				controller: 'gameViewCtrl',
                auth: false
			})
	});