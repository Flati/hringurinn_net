angular.module('lan.game').controller('gameViewCtrl', function ($http, $scope, $stateParams, $sce, marked) {
    var id = $stateParams.id;

    function _getGameDetails(id) {
        $http.get('/api/game/' + id).success(function (res) {
            $scope.game = res;
            $scope.rules = $sce.trustAsHtml(marked('#TEST'));
            //$scope.rules = $sce.trustAsHtml(res.rules);
        });
    }

    _getGameDetails(id);



});