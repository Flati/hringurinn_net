angular.module('lan.dashboard').controller('dashboardCtrl', function ($scope, gameService) {
    gameService.getAll().then(function (games) {
        $scope.games = games;
    })
});