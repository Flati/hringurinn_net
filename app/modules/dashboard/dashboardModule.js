angular.module('lan.dashboard',[])
	.config(function($stateProvider){
		$stateProvider
			.state('dashboard',{
				url: '/',
				templateUrl: '/assets/modules/dashboard/partials/dashboard.html',
				controller: 'dashboardCtrl'
			})
	});