angular.module('lan.common').factory('authService', function (userService, $rootScope, $state, toastrService) {

    auth = {};

    var authenticationPromise = undefined;

    auth.checkIfAuthenticated = function () {
        authenticationPromise = userService.me()
            .then(function (user) {
                $rootScope.$broadcast('authenticated');
            });
    };

    $rootScope.$on('$stateChangeStart', function (event, toState) {
        if(!authenticationPromise){
            auth.checkIfAuthenticated();
        }

        authenticationPromise.finally(function(){
            if (toState.auth && !$rootScope.user) {
                event.preventDefault();
                toastrService.info('Þú verður að vera innskráður til að skoða þessa síðu');
                $state.go('user.login');
            }
        })


    });

    return auth;
});