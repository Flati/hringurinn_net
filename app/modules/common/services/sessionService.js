angular.module('lan.common').service('sessionService', function ($rootScope, $http) {


    function login(loginObj) {
        return $http.post('/api/session', loginObj)
            .then(function (res) {
                $rootScope.$emit('authenticated');
                return res.data;
            });
    }

    function logout() {
        return $http.delete('/api/session')
            .then(function () {
                $rootScope.user = undefined;
                $rootScope.$emit('unauthenticated');
                return true;
            });
    }

    return {
        login: login,
        logout: logout
    }
});