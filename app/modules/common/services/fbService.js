angular.module('lan.common').factory('fbService',
    function ($q) {

        /**
         * Initialization
         */

        var _token = null;

        // Append facebook tag to index
        $('body').append('<div id="fb-root"><\/div>');

        /**
         * Service functions
         */

        var initStatus = undefined;

        // Called when service initalizes
        function init() {

            if (!initStatus) {
                initStatus = $q.defer();
                $.ajaxSetup({ cache: true });
                $.getScript('https://connect.facebook.net/en_UK/all.js', function () {
                    //FB.init(options);
                    initStatus.resolve();
                });
            }
            return initStatus.promise;
        }

        function graph(method) {
            var deferred = $q.defer();
            init().then(function () {
                FB.api(method, {access_token: _token}, function (response) {
                    deferred.resolve(response);
                });
            });
            return deferred.promise;
        }

        function setToken(token) {
            _token = token;
        }

        return {
            graph: graph,
            setToken: setToken
        }
    });