angular.module('lan.common').service('navbarService', function () {
	this.options = {
		title: 'HRingurinn 2015',
		links: [
			{text: 'Heim', state: 'dashboard'},
			{text: 'Samstarfsaðilar', state: 'partners'},
			{text: 'Algengar spurningar', state: 'faq'},
			{text: 'Lið', state: 'team.list', auth: true},
			{text: 'Greiðsla', state: 'user.payment', auth: true}
		]
	}
});