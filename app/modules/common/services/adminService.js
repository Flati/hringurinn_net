angular.module('lan.common').factory('adminService', function ($http) {
    var as = {};

    as.getSettings = function () {
        return $http.get('/api/settings').then(function(res){
            res.data.closeRegistration = moment(res.data.closeRegistration);
            res.data.openRegistration = moment(res.data.openRegistration);
            return res.data;
        });
    };

    return as;
});