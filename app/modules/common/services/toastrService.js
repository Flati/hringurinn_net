angular.module('lan.common').provider('toastrService', function () {

    this.options = function (opt) {
        // add new options
        angular.extend(toastr.options, opt);
    };

    this.$get = function () {
        return toastr;
    };

});