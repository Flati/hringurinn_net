angular.module('lan.common').factory('userService',
    function ($http, $rootScope, fbService) {
        var us = {};

        us.register = function (form) {
            return $http.post('/api/users', form)
                .then(function () {
                    return us.me();
                });
        };

        us.linkToFacebook = function () {
            return fbService.graph('/me')
                .then(function (res) {
                    return $http.post('/api/user/link/facebook', res);
                })
                .then(function (fbID) {
                    $rootScope.user.facebook = fbID;
                })

        };

        us.exists = function (email, ssn) {
            return $http.get('/api/users/exists/' + ssn + '/' + email);
        };

        us.me = function () {
            return $http.get('/api/users/me')
                .then(function (res) {
                    $rootScope.user = res.data;
                }, function () {
                    $rootScope.user = false;
                });
        };

        us.getAll = function () {
            return $http.get('/api/users')
                .then(function (res) {
                    return res.data;
                });
        };


        us.passwordReset = {
            requestCode: function (email) {
                return $http.post('/api/users/password/reset', {email: email})
                    .then(function (res) {
                        return res.data;
                    })
            },
            changePassword: function (code, password) {
                return $http.put('/api/users/password/reset', {code: code, password: password})
                    .then(function (res) {
                        return res.data;
                    });
            }
        };

        us.changePassword = function (passwordObj) {
            return $http.put('/api/users/password', passwordObj)
                .then(function (res) {
                    return res.data;
                });
        };

        us.deleteUser = function (id) {
            return $http.delete('/api/users/' + id)
                .then(function (res) {
                    return res.data;
                });
        };

        /**
         * Event Handlers
         */

        $rootScope.$on('authenticated', function () {
            us.me();
        });

        $rootScope.$on('team', function (event, act) {
            if (act.action === 'create') {
                us.me();
            }
            else if (act.action === 'join') {
                us.me();
            }
            else if (act.action === 'remove') {
                us.me();
            }

        });

        return us;
    });