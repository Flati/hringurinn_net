angular.module('lan.common').directive('navbar',function(navbarService, sessionService, $state, config){
	return {
		templateUrl: '/assets/modules/common/directives/navbar/navbar.html',
		restrict: 'E',
		scope: {

		},
		link: function($scope, $element, $attrs){
			$scope.options = navbarService.options;
            $scope.url = config.url;

			$scope.logout = function(){
				sessionService.logout().then(function(){
					$state.go('dashboard');
				});
			};
		}
	}
});