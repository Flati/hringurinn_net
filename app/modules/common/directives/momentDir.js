angular.module('lan.common').filter('moment',function(){
    return function(input){
        return moment(input).format("dddd, MMMM Do YYYY, hh:mm:ss");
    }
});