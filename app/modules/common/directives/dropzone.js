angular.module('lan.common').directive('dropzone', function () {
    return {
        scope: {
          options: '=?'
        },
        link: function ($scope, $element) {

            var defaults = {
                url: "/upload",
                maxFilesize: 2,
                paramName: "file",
                maxThumbnailFilesize: 2,
                autoProcessQueue: false
            };

            // Overwrite the default options
            if($scope.options){
                angular.extend(defaults, $scope.options);
            }

            $element.dropzone(defaults);
        }
    }
});