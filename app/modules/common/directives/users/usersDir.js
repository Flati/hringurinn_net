angular.module('lan.common').directive('users', function (userService) {
    return {
        restrict: 'EA',
        templateUrl: '/assets/modules/common/directives/users/users.html',
        scope: {
            onSelect: '&'
        },
        link: function ($scope, $attrs, $element) {
            var _users = [];
            $scope.selected= null;

            userService.getAll().then(function(users){
                $scope.users = users;
                _users = users;
            });

            $scope.search = _.debounce(function(term){
                this.$apply(function(){
                    $scope.users = search(term);
                });
            },50);

            $scope.select = function(user){
                $scope.onSelect()(user);
            };

            function search(term){
                term = term.toLowerCase();
                return _.filter(_users, function(user){
                    if(user.name.toLowerCase().indexOf(term) !== -1){
                        return user;
                    }
                    if(user.ssn.indexOf(term) !== -1){
                        return user;
                    }
                })
            }
        }
    }
});