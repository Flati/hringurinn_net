angular.module('lan.common').directive('fileUploader', function () {
    return {
        restrict: 'E',
        scope: {
            file: '='
        },
        template: '<input type="file"/>',
        link: function ($scope, $element) {
            $element.bind('change', function (e) {
                console.log(e.target.files);
                //console.log(e.target.files[0])
                $scope.$apply(function(){
                    console.log(e.target.files[0].name);
                    $scope.file = e.target.files[0];
                })
            });

            $scope.$on('$destroy', function() {
                $element.unbind()
            });
        }
    }
});