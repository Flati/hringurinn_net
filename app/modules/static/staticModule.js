angular.module('lan.static', [])
    .config(function ($stateProvider) {
        $stateProvider
            .state('static', {
                url: '/static',
                abstract: true,
                template: '<div ui-view></div>'
            })
            .state('static.terms', {
                url: '/terms',
                templateUrl: '/assets/modules/static/partials/terms.html'
            })
            .state('static.under18', {
                url: '/under18',
                templateUrl: '/assets/modules/static/partials/under18.html'
            });
    });