angular.module('lan.user',[])
	.config(function($stateProvider){
		$stateProvider
			.state('user',{
				url: '/user',
				abstract: true,
				template:'<div ui-view></div>'
			})
			.state('user.login',{
				url: '/login/:callback',
				templateUrl:'/assets/modules/user/partials/login.html',
				controller: 'loginCtrl'
			})
            .state('user.forgotPassword',{
                url: '/forgotPassword/:code',
                templateUrl:'/assets/modules/user/partials/forgotPassword.html',
                controller: 'forgotPasswordCtrl'
            })
			.state('user.me',{
				url: '/me/:callback',
				templateUrl:'/assets/modules/user/partials/me.html',
				controller: 'meCtrl'
			})
            .state('user.payment',{
                url: '/payment',
                templateUrl:'/assets/modules/user/partials/payment.html',
                controller: 'paymentCtrl'
            })
			.state('user.register',{
				url: '/register',
				templateUrl: '/assets/modules/user/partials/register.html',
				controller: 'registerCtrl'
			})
			.state('user.register.provider',{
				url: '/',
				templateUrl: '/assets/modules/user/partials/register.provider.html'
			})
			.state('user.register.basic',{
				url: '/basic',
				templateUrl: '/assets/modules/user/partials/register.basic.html'
			})
            .state('user.register.info',{
                url: '/info/:callback',
                templateUrl: '/assets/modules/user/partials/register.info.html'
            })
	});