angular.module('lan.user').controller('forgotPasswordCtrl',
    function ($scope, userService, toastrService, $state) {

        $scope.requestCode = function (form) {
            userService.passwordReset.requestCode(form.email)
                .then(function () {
                    toastrService.success('Tölvupóstur með nánari upplýsingum sendur');
                }, function () {
                    toastrService.error('Ekki tókst að senda tölvupóst til þess að endursetja lykilorð');
                });
            $scope.state = 'done';
        };


        if ($state.params.code) {
            $scope.state = 'password'
        }

        $scope.changePassword = function (form) {
            userService.passwordReset.changePassword($state.params.code, form.password)
                .then(function () {
                    toastrService.success('Tókst að breyta lykilorði, Vinsamlegast skráðu þig inn.');
                    $state.go('user.login')
                }, function () {
                    toastrService.error('Ekki tókst að breyta lykilorði');
                });

        }

    });