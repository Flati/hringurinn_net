angular.module('lan.user').controller('meCtrl',
    function (userService, $scope, $modal, toastrService, config, fbService, $state, $window, $location) {
        $scope.form = {};

        $scope.linkToFacebook = function () {
            $window.location.replace('https://www.facebook.com/dialog/oauth?client_id=1417743428473625&response_type=code%20token&redirect_uri=' + config.url + '/user/me/true&scope=email');
        };

        if ($state.params.callback) {
            // Fetch token
            var tmp = window.location.href;
            var tokenRE = /access_token=(.*)&expires_in/;
            var token = tokenRE.exec(tmp)[1];

            fbService.setToken(token);
            userService.linkToFacebook()
                .then(function () {
                    toastrService.success('Tókst að tengja við Facebook');
                }, function () {
                    toastrService.error('Tókst ekki að tengja við Facebook');
                });

        }


        $scope.changePassword = function () {
            var modalInstance = $modal.open({
                templateUrl: '/assets/modules/user/partials/changePassword.html',
                controller: 'changePasswordModelCtrl'
            });

            modalInstance.result.then();
        };

        $scope.changePicture = function () {
            var modalInstance = $modal.open({
                templateUrl: '/assets/modules/user/partials/changePicture.html',
                controller: 'changePictureModelCtrl'
            });

            modalInstance.result.then();
        };

        $scope.deleteUser = function () {
            userService.deleteUser('me')
                .then(function () {
                    // Go to the front-page and refresh so we loose the session
                    $window.location.href = '/';
                }, function () {
                    toastrService.error('Mistókst að eyða notanda');
                });
        };
    })
    .controller('changePasswordModelCtrl', function ($scope, $modalInstance, toastrService, userService) {

        $scope.changePassword = function (form) {
            userService.changePassword(form)
                .then(function () {
                    toastrService.success('Tókst að breyta um lykilorð');
                    $modalInstance.close();
                }, function () {
                    toastrService.error('Mistókt að breyta um lykilorð. Er núverandi lykilorð rétt?')
                });
        };

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        }
    })
    .controller('changePictureModelCtrl', function ($scope, $modalInstance) {

        $scope.onFileSelect = function ($file) {
            var file = $file[0];
            console.log(file);
            // Get the file size
            if ((file.size / 1048576) > 2) {
                return console.log('image to big');
            }
            var allowedFileTypes = ['image/png', 'image/jpg', 'image/jpeg'];
            var allowed = false;
            for (var i = 0; i < allowedFileTypes.length; ++i) {
                if (allowedFileTypes[i] === file.type) {
                    allowed = true;
                }
            }
            console.log('allowed ' + allowed);
            // Generate the preview
            var reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = function (event) {
                var img = new Image();
                img.src = event.target.result;

                $scope.$apply(function () {
                    $scope.preview = event.target.result;
                });
            };
        };

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        }
    });