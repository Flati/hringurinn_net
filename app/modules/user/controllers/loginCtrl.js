angular.module('lan.user').controller('loginCtrl',
    function ($scope, sessionService, $state, fbService, toastrService, userService, $window, config) {
        $scope.show = true;

        $scope.login = function (model) {
            sessionService.login({email: model.email, password: model.password})
                .then(function () {
                    return userService.me();
                })
                .then(function () {
                    $state.go('team.list');
                }, function () {
                    toastrService.error('Vitlaust email eða lykilorð');
                })
        };

        if($state.params.callback){
            $scope.show = false;
            var tmp = window.location.href;
            var tokenRE = /access_token=(.*)&expires_in/;
            var token = tokenRE.exec(tmp)[1];
            sessionService.login({ access_token: token })
                .then(function(){
                    return userService.me();
                })
                .then(function () {
                    $state.go('team.list');
                }, function () {
                    $state.reload();
                    $scope.show= true;
                    toastrService.error('Gat ekki innskráð í gegnum Facebook, ertu viss um að notandinn sé til?');
                });
        }

        $scope.facebook = function () {
            $window.location.replace('https://www.facebook.com/dialog/oauth?client_id=1417743428473625&response_type=token&redirect_uri='+config.url+'/user/login/true&scope=email');
        };
    });