angular.module('lan.user').controller('registerCtrl',
    function ($scope, userService, fbService, $state, toastrService, $window, config) {

        $scope.form = {};

        $scope.facebook = function () {
            $window.location.replace('https://www.facebook.com/dialog/oauth?client_id=1417743428473625&response_type=code%20token&redirect_uri='+config.url+'/user/register/info/true&scope=email');
        };

        if($state.params.callback){
            var tmp = window.location.href;
            var tokenRE = /access_token=(.*)&expires_in/;
            $scope.form.token = tokenRE.exec(tmp)[1];
            var codeRE = /code=(.*)/;
            $scope.form.code = codeRE.exec(tmp)[1];

            fbService.setToken($scope.form.token);
            fbService.graph('/me').then(function(response){
                $scope.form.name = response.name;
            })
        }

        $scope.register = function (form) {
            userService.register(form)
                .then(function () {
                    $state.go('team.list');
                }, function () {
                    toastrService.error('Skráning mistókst');
                });
        };

        $scope.nextPage = function (form) {
            userService.exists(form.email)
                .then(function (res) {
                    if (res.data.exists) {
                        return toastrService.error('Notandi er nú þegar skráður');
                    }
                    else {
                        $state.go('user.register.info');
                    }
                });
        };

    });