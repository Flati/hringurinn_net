'use strict';

angular.module('lan.user').directive('equals', function () {
    return {
        require: 'ngModel',
        scope: {
            equals: '='
        },
        link: function ($scope, $element, $attrs, $ngModel) {
            $ngModel.$parsers.unshift(function (value) {

                if (!$ngModel.$isEmpty(value) && $scope.equals === value) {

                    $ngModel.$setValidity('equals', true);
                    return value;
                }

                $ngModel.$setValidity('equals', false);
                return undefined;

            });

        }
    };
});