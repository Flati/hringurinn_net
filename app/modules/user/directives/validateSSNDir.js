'use strict';

angular.module('lan.user').directive('validateSsn', function () {
    return {
        require: 'ngModel',
        link: function ($scope, $element, $attrs, $ngModel) {
            $ngModel.$parsers.unshift(function (value) {

                if (!$ngModel.$isEmpty(value) && value.length === 10) {
                    var checkNr = [3, 2, 7, 6, 5, 4, 3, 2];
                    var kt = value.split('').map(function (num) {
                        return parseInt(num,10);
                    });

                    if(kt[0] > 3 || kt[2] > 1){
                        $ngModel.$setValidity('ssn', false);
                        return undefined;
                    }

                    var num = 0;
                    for (var i = 0; i < 8; ++i) {
                        num += kt[i] * checkNr[i];
                    }
                    var checkNum = (num % 11) === 0 ? (num % 11) :  11 - (num % 11) ;

                    if (kt[8] === checkNum) {
                        $ngModel.$setValidity('ssn', true);
                        return value;
                    }
                    else {
                        $ngModel.$setValidity('ssn', false);
                        return undefined;
                    }
                }
            });

        }
    };
});