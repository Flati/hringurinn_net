angular.module('lan.team').controller('teamListCtrl',
    function ($scope, $state, teamService, toastrService, $rootScope) {

        $scope.viewTeam = function (teamId) {
            if(typeof $rootScope.user.team !== 'undefined'){
                if(($rootScope.user.team._id === teamId) && !$rootScope.user.team.isApplicant){
                    $state.go('team.view', { id: teamId });
                }
            }
        };

        $scope.joinTeam = function (event,teamId) {
            teamService.join(teamId, 'me')
                .then(function(){
                    //TODO: Should we go inside the team view or display a button?
                    toastrService.success('Þú hefur sótt um í lið');
                }, function(){
                    toastrService.error('Eitthvað fór úrskeiðis');
                });
            event.stopPropagation();
        };

    });