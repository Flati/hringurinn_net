angular.module('lan.team').controller('teamCtrl', function ($scope, teamService, $rootScope) {

    teamService.getAll()
        .then(function (teams) {
            $scope.teams = teams;
        });


    /**
     * Event handlers
     */

    $rootScope.$on('team', function () {
        teamService.getAll()
            .then(function (teams) {
                $scope.teams = teams;
            });
    });

});