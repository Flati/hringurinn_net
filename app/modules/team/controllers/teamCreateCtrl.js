angular.module('lan.team').controller('teamCreateCtrl',
    function ($scope, teamService, gameService, toastrService, $state) {

        gameService.getAll().then(function (games) {
            $scope.games = games;
        });


        $scope.createTeam = function (team) {
            teamService.create(team)
                .then(function (newTeam) {

                    toastrService.success('Tókst að búa til lið');
                    $state.go('team.view', {id: newTeam._id});

                }, function () {

                    toastrService.error('Eitthvað fór úrskeiðis við skráningu liðs');

                });
        };

    });