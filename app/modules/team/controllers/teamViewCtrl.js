angular.module('lan.team').controller('teamViewCtrl',
    function ($scope, teamService, $stateParams, $modal, toastrService, $state, $rootScope) {

        var id = $stateParams.id;

        $scope.error = {
            show: false,
            message: ''
        };

        $scope.getTeam = function () {
            teamService.getById(id).then(function (team) {

                $scope.team = _prepRows(_addEmptySlots(team));

            }, function () {
                toastrService.error('Ekkert lið fannst');
                $state.go('team.list');
            })
        };

        $scope.getTeam();


        function _addEmptySlots(team) {
            var numberOfMinPlayers = team.game.minNumberOfPlayers;
            $scope.currentPlayers = team.members.length;
            var numberToAdd = numberOfMinPlayers - $scope.currentPlayers;
            if(numberToAdd > 0){
                for (var i = 0; i < numberToAdd; ++i) {
                    team.members.push({ placeholder: true });
                }
            }
            return team;
        }


        function _prepRows(team) {
            var numberOfRows = Math.ceil(team.members.length / 2);
            var members = team.members;
            team.members = [];
            // Create rows
            for (var i = 0; i < numberOfRows; ++i) {
                team.members.push([]);
            }

            // Insert into rows
            while (members.length > 0) {
                for (var b = 0; b < numberOfRows; ++b) {
                    while (team.members[b].length < 2 && (members.length > 0)) {
                        team.members[b].push(members.shift());
                    }
                }
            }
            return team;
        }


        $scope.quitTeam = function (teamId, userId) {
            teamService.removeMember(teamId, userId)
                .then(function () {
                    $state.go('team.list');
                });
        };


        $scope.approveInTeam = function (teamId, userId) {
            teamService.approveMember(teamId, userId)
                .then(function () {
                    $scope.getTeam();
                });
        };


        $scope.deleteTeam = function () {
            var modalInstance = $modal.open({
                templateUrl: '/assets/modules/team/partials/deleteModal.html',
                controller: 'deleteModalCtrl'
            });

            modalInstance.result.then(function (reason) {
                if (reason === 'remove') {
                    teamService.remove(id)
                        .then(function () {
                            toastrService.success('Liði eytt');
                            $state.go('team.list');
                        },
                        function () {
                            toastrService.error('Mistókst að eyða liði');
                        });
                }
            });
        };

    })


    .controller('deleteModalCtrl', function ($scope, $modalInstance) {

        $scope.confirm = function () {
            $modalInstance.close('remove');
        };

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        }
    });