angular.module('lan.team',[])
	.config(function($stateProvider){
		$stateProvider
			.state('team',{
				url: '/team',
				template: '<ui-view></ui-view>',
				controller: 'teamCtrl',
				abstract: true
			})
			.state('team.list',{
				url: '/list',
				templateUrl: '/assets/modules/team/partials/list.html',
				controller: 'teamListCtrl',
                auth: true
			})
			.state('team.create',{
				url: '/create',
				templateUrl: '/assets/modules/team/partials/create.html',
				controller: 'teamCreateCtrl',
                auth : true
			})
			.state('team.view',{
				url: '/:id',
				templateUrl: '/assets/modules/team/partials/view.html',
				controller: 'teamViewCtrl',
                auth: true
			})
	});