angular.module('lan.team').directive('teamMember', function (teamService, toastrService, $rootScope) {
    return {
        restrict: 'EA',
        templateUrl: '/assets/modules/team/directives/teamMember.html',
        scope: {
            member: '=',
            refresh: '&'
        },
        link: function ($scope, $attrs, $element) {
            $scope.removeMember = function (teamId,member) {
                teamService.removeMember(teamId,member._id)
                    .then(function () {
                        $scope.refresh();
                        toastrService.success('Tókst að fjarlægja notanda');
                    }, function () {
                        toastrService.error('Mistókt að fjarlægja notanda');
                    })
            }
        }
    }
});