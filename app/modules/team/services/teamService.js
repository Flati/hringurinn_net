angular.module('lan.team').factory('teamService',
    function ($rootScope, $http) {


        function getAll() {
            return $http.get('/api/team')
                .then(function (res) {
                    return res.data;
                })
        }

        function getById(id) {
            return $http.get('/api/team/' + id)
                .then(function (res) {
                    return res.data;
                })
        }

        function create(team) {
            return $http.post('/api/team', team)
                .then(function (res) {
                    $rootScope.$emit('team', { action: 'create', data: res.data });
                    return res.data;
                })
        }

        function remove(teamId) {
            return $http.delete('/api/team/' + teamId)
                .then(function (res) {
                    $rootScope.$emit('team', { action: 'remove', data: teamId});
                    return res.data;
                })
        }

        function join(teamId, userId) {
            return $http.post('/api/users/' + userId + '/team/' + teamId)
                .then(function(team){
                    $rootScope.$emit('team', { action: 'join'});
                })
        }

        function removeMember(teamId, userId) {
            return $http.delete('/api/users/' + userId + '/team/' + teamId)
                .then(function (res) {
                    $rootScope.$emit('team', { action: 'remove', data: {
                        teamId: teamId,
                        userId: userId
                    }});
                    return res.data;
                })
        }


        function approveMember(teamId, userId) {
            return $http.put('/api/users/' + userId + '/team/' + teamId)
                .then(function (res) {
                    $rootScope.$emit('team', { action: 'user.approve', data: {
                        teamId: teamId,
                        userId: userId
                    }});
                    return res.data;
                })
        }

        return {
            getAll: getAll,
            getById: getById,
            create: create,
            remove: remove,
            approveMember: approveMember,
            removeMember: removeMember,
            join: join
        }
    });