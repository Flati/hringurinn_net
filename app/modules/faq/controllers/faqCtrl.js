angular.module('lan.faq').controller('faqCtrl', function ($scope, $http, toastrService) {

    function getFaqs() {
        $http.get('/api/faq')
            .success(function (data) {
                $scope.faqs = data;
            })
    }

    getFaqs();

    $scope.form = {};

    $scope.createFaq = function(form) {
        $http.post('/api/faq', form)
            .success(function () {
                getFaqs();
                toastrService.success('Tókst að bæta við');
                $scope.form = {};
            }, function(){
                toastrService.error('Mistókt að bæta við');
            });
    };

    $scope.removeFaq = function(id){
        $http.delete('/api/faq/'+id)
            .success(function () {
                getFaqs();
                toastrService.success('Tókst að eyða');
            }, function(){
                toastrService.error('Mistókt að eyða');
            });
    };

});