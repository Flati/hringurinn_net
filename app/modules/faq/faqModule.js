angular.module('lan.faq',[])
	.config(function($stateProvider){
		$stateProvider
			.state('faq',{
				url: '/faq',
				templateUrl:'/assets/modules/faq/partials/list.html',
				controller: 'faqCtrl'
			});
	});