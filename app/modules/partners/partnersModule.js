angular.module('lan.partners',[])
	.config(function($stateProvider){
		$stateProvider
			.state('partners',{
				url: '/partners',
				templateUrl:'/assets/modules/partners/partials/view.html',
				controller: 'partnersCtrl'
			})

	});