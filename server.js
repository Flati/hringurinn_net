'use strict';

var express = require('express'),
	path = require('path'),
	fs = require('fs'),
	mongoose = require('mongoose'),
    logger = require('./server/logger');

/**
 * Main application file
 */

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Application Config
var config = require('./server/config/config');

// Connect to database
var db = mongoose.connect(config.mongo.uri, config.mongo.options);
logger.log('info', 'Connecting to DB server '+ config.mongo.uri);

// Bootstrap models
var modelsPath = path.join(__dirname, 'server/models');
fs.readdirSync(modelsPath).forEach(function (file) {
	if (/(.*)\.(js$|coffee$)/.test(file)) {
		require(modelsPath + '/' + file);
	}
});

// Populate empty DB with sample data
require('./server/config/dummydata');

// Passport Configuration
var passport = require('./server/config/passport');

var app = express();

// Express settings
require('./server/config/express')(app);

// Routing
require('./server/routes')(app);

// Winston logger


// Start server
app.listen(config.port, function () {
	console.log('Express server listening on port %d in %s mode', config.port, app.get('env'));
    logger.log('info', 'Starting server on port  '+ config.port + ' env ' + app.get('env'));
});

// Expose app
exports = module.exports = app;